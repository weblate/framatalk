[![](https://framagit.org/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

Framatalk est un service en ligne de visio-conférence que [Framasoft](https://framasoft.org) propose sur le site : <https://framatalk.org>.

Il repose sur le logiciel [Jitsi Meet](https://jitsi.org/Projects/JitsiMeet) que nous n'avons pas modifié.
La page d'accueil s'appuie sur le framework CSS Bootstrap dont les fichiers se trouvent dans la [Framanav](https://framagit.org/framasoft/framanav).

Si vous souhaitez traduire la page d’accueil, allez sur <https://weblate.framasoft.org/projects/framatalk/homepage/>.

* * *

Framatalk is an visio-conference online service provided by [Framasoft](https://framasoft.org) on <https://framatalk.org>.

It’s based on [Jitsi Meet](https://jitsi.org/Projects/JitsiMeet) that we didn’t modify.
The homepage uses the CSS framework Bootstrap which you can find files in the [Framanav](https://framagit.org/framasoft/framanav).

If you want to translate the homepage, go on <https://weblate.framasoft.org/projects/framatalk/homepage/>.
